#
# Copyright (C) 2021 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/awaken_psyche.mk

COMMON_LUNCH_CHOICES := \
    awaken_psyche-eng \
    awaken_psyche-userdebug \
    awaken_psyche-user
